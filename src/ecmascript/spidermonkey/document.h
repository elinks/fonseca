
#ifndef EL__ECMASCRIPT_SPIDERMONKEY_DOCUMENT_H
#define EL__ECMASCRIPT_SPIDERMONKEY_DOCUMENT_H

#include "ecmascript/spidermonkey/util.h"

extern const JSClass document_class;
extern const JSFunctionSpec document_funcs[];
extern const JSPropertySpec document_props[];

#endif
