/* Tool for testing the DOM box formatter */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "elinks.h"

#include "document/docdata.h"
#include "dom/format.h"
#include "dom/node.h"
#include "dom/sgml/dump.h"
#include "dom/sgml/html/html.h"
#include "dom/sgml/parser.h"
#include "dom/stack.h"
#include "util/test.h"


static struct dom_string *display;
static size_t display_height;

static int opt_show_line_numbers;

static const unsigned char usage[] =
"dom-formatter [options] [--] [SGML code] < [SGML code]\n"
"\n"
"Options:\n"
"  --show-line-numbers           Prefix line numbers for each line\n"
"";

static void
render_dom_segment(void *data, struct dom_segment *segment)
{
	union dom_segment_value *value = &segment->value;
	struct dom_string *line;
	size_t max_y = segment->offset.top;
	size_t max_x = segment->offset.left + value->text.length;

	if (display_height <= max_y) {
		if (!ALIGN_LINES(&display, display_height, max_y + 1))
			die("failed to reallocate display");
		display_height = max_y + 1;
	}

	line = &display[max_y];

	if (line->length < max_x) {
		if (!ALIGN_LINE(&line->string, line->length, max_x + 1))
			die("failed to reallocate display");
		memset(line->string + line->length, ' ', max_x - line->length);
		line->length = max_x;
	}

	switch (segment->type) {
	case DOM_SEGMENT_CHARRUN:
		memcpy(line->string + segment->offset.left,
		       value->text.string, value->text.length);
		break;

	default:
		die("Unknown segment type");
		break;
	}
}

static void
print_display(void)
{
	size_t i;

	if (!display)
		return;

	for (i = 0; i < display_height; i++) {
		struct dom_string *line = &display[i];

		if (opt_show_line_numbers)
			printf("%3u |", i);

		if (line->string) {
			printf("%s", line->string);
			mem_free(line->string);
		}

		printf("\n");
	}

	mem_free(display);
}

static enum dom_code
parse_sgml_from_stdin(struct sgml_parser *parser, size_t read_size)
{
	enum dom_code code = DOM_CODE_OK;
	unsigned char *buffer;
	int complete = 0;

	buffer = mem_alloc(read_size);
	if (!buffer)
		die("Cannot allocate buffer");

	while (!complete) {
		size_t size = fread(buffer, 1, read_size, stdin);

		if (ferror(stdin))
			die("error reading from stdin");

		complete = feof(stdin);

		code = parse_sgml(parser, buffer, size, complete);
		switch (code) {
		case DOM_CODE_OK:
			break;

		case DOM_CODE_INCOMPLETE:
			if (!complete) break;
			/* Error */
		default:
			complete = 1;
		}
	}

	mem_free(buffer);
	return code;
}

static enum dom_code
parse_sgml_from_args(struct sgml_parser *parser, int i, int argc, char **argv)
{
	for (; i < argc; i++) {
		switch (parse_sgml(parser, argv[i], strlen(argv[i]), i == argc - 1)) {
		case DOM_CODE_OK:
			break;

		case DOM_CODE_INCOMPLETE:
			if (i < argc - 1)
				break;

			/* Error */
		default:
			return DOM_CODE_ERR;
		}
	}

	return DOM_CODE_OK;
}

int
main(int argc, char *argv[])
{
	struct dom_string uri = STATIC_DOM_STRING("dom://formatter");
	struct sgml_parser *parser;
	enum dom_code code = 0;
	int i;

	for (i = 1; i < argc; i++) {
		char *arg = argv[i];

		if (strncmp(arg, "--", 2))
			break;

		arg += 2;

		if (!strcmp(arg, "show-line-numbers")) {
			opt_show_line_numbers = 1;

		} else if (!strcmp(arg, "help")) {
			die("%s [--show-line-numbers] [SGML code] < [HTML code]", argv[0]);

		} else {
			die("Unknown argument '%s'\n\n%s", arg - 2, usage);
		}
	}

	if (i == argc && isatty(STDIN_FILENO))
		die("%s [SGML code] < [HTML code]", argv[0]);

	parser = init_sgml_parser(SGML_PARSER_TREE, SGML_DOCTYPE_HTML, &uri, SGML_PARSER_INCREMENTAL);
	if (!parser)
		die("Failed to create parser");

	if (!isatty(STDIN_FILENO)) {
		code = parse_sgml_from_stdin(parser, 4096);
		if (code != DOM_CODE_OK)
			die("Parsing from stdin failed");
	}

	code = parse_sgml_from_args(parser, i, argc, argv);
	if (code != DOM_CODE_OK)
		die("Parsing arguments failed");

	if (parser->root) {
		format_dom(parser->root, render_dom_segment, NULL);
		print_display();
	}

	done_dom_node(parser->root);
	done_sgml_parser(parser);
#ifdef DEBUG_MEMLEAK
	check_memory_leaks();
#endif

	return EXIT_SUCCESS;
}
