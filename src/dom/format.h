#ifndef EL_DOM_FORMAT_H
#define EL_DOM_FORMAT_H

/** DOM formatter
 *
 * @file
 *
 * The DOM formatter works on a DOM tree or part of a DOM tree denoted
 * by a node. It traverses the DOM tree and formats it into segments
 * that are passed to the renderer via a callback.
 *
 * The formatter is grid oriented, in that it assumes all characters
 * have the same width.
 */

#include "dom/string.h"
#include "dom/style.h"
#include "util/color.h"

struct dom_node;

/** DOM segment type
 *
 * The segment type specifies what kind of content the renderer should
 * expect. The primary type is character runs (#DOM_SEGMENT_CHARRUN)
 * for rendering text.
 *
 * FIXME: Future segment type should include stuff like:
 *
 *  - DOM_SEGMENT_AREA: Along with character runs this type is another
 *    primitive segment for filling and area of the document grid with
 *    stuff like background color.
 *  - DOM_SEGMENT_IMAGE: Render image box
 *  - DOM_SEGMENT_OBJECT: Render (media) object box
 *  - DOM_SEGMENT_FORM_*: Form fields
 *  - DOM_SEGMENT_FRAME: Inline frame
 *
 * FIXME: Segments should maybe also be used for the equivalent of the
 * html_special callback in the current HTML renderer.
 */
enum dom_segment_type {
	DOM_SEGMENT_CHARRUN,
};

/** DOM segment type specific values. */
union dom_segment_value {
	struct dom_string text;		/**< Character run value. */
};

/** DOM formatted segment
 *
 * Segments are atomic units of content to be updated on the character
 * grid of the document canvas. Each segment can represent either a fill
 * area for updating a matrix of characters with a set of attributes
 * (such as border graphics or background colors) or a character run.
 */
struct dom_segment {
	enum dom_segment_type type;	/**< The segment type. */
	union dom_segment_value value;	/**< Type specific values. */

	struct dom_node *element;	/**< The containing element node. */
	struct dom_style *style;	/**< Style information. */

	/** The position in the document grid and dimension of the segment.
	 *
	 * The offset.top and offset.left members contains the coordinate of
	 * the segment upper left corner. The offset.bottom and offset.right
	 * members contain the coordinates of the segment upper left corner.
	 */
	struct dom_area offset;
};

/** The render callback for handling formatted segments
 *
 * The formatter uses this callback to render each formatted segment.
 * The renderer should consider all segments representing a character
 * run non-wrapping line boxes, meaning that all whitespace in the
 * strings has been preformatted and should be copied to the display
 * verbatim.
 *
 * @param data      Renderer specific data, as passed to #format_dom.
 * @param segment   The segment to be rendered.
 */
typedef void (*dom_render_segment_T)(void *data, struct dom_segment *segment);

/** Format a DOM tree
 *
 * Traverse a DOM tree using the given render callback (along with the
 * provided render data) to inform the renderer about new segments.
 */
void format_dom(struct dom_node *root, dom_render_segment_T fn, void *data);

#endif
