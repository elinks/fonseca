#ifndef EL_DOM_STYLE_H
#define EL_DOM_STYLE_H

/** DOM formatting styles.
 *
 * @file
 */

#include "util/color.h"


enum dom_style_display {
	DOM_STYLE_DISPLAY_NONE,
	DOM_STYLE_DISPLAY_BLOCK,
	DOM_STYLE_DISPLAY_LIST_ITEM,
	DOM_STYLE_DISPLAY_INLINE,
	DOM_STYLE_DISPLAY_COMPACT,
	DOM_STYLE_DISPLAY_MARKER,
	DOM_STYLE_DISPLAY_RUN_IN,
	DOM_STYLE_DISPLAY_TABLE,
	/* ... and more. */
};

enum dom_style_white_space {
	DOM_STYLE_WHITE_SPACE_PRE,
	DOM_STYLE_WHITE_SPACE_NOWRAP,
	DOM_STYLE_WHITE_SPACE_NORMAL,
};

struct dom_area {
	unsigned int top;
	unsigned int bottom;
	unsigned int left;
	unsigned int right;
};

struct dom_style {
	enum dom_style_display display;
	enum dom_style_white_space white_space;
	struct dom_area margin;
	struct dom_area padding;
	struct dom_area border;
	struct dom_string content;
	struct color_pair color;
};

#endif
