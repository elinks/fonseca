/* DOM formatter */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <string.h>

#include "elinks.h"

#include "dom/configuration.h"
#include "dom/format.h"
#include "dom/node.h"
#include "dom/stack.h"
#include "dom/style.h"

/** DOM formatting box
 *
 * This is the private stack context, allocated for all nodes pushed on
 * the stack, however, it is only in effect for block-level boxes. For
 * example, text nodes must refer to the dom_formatter_box offset of the
 * containing box.
 *
 * Each time a block-level element is encountered a new state is pushed
 * on the formatter's stack of (block) boxes. By referring to the offset
 * and position coordinates of its parent's block box (also called its
 * containing box) it can update its own box_offset.
 */
struct dom_formatter_box {
	/** Backward index pointer to the state of the containing block. */
	unsigned int parent;

	/** Element node. */
	struct dom_node *element;

	/** Element style information. */
	struct dom_style *style;

	/** Offset on the canvas for the box. */
	struct dom_area offset;

	/** Coordinates for the current position in the box.
	 *
	 * The coordinates are relative to the offsets in the box_offset
	 * members. Together they denote the absolute position in the
	 * document character grid. */
	unsigned int x, y;

	/** Maximum width of the box.
	 *
	 * After formatting text the maximum width of the containing box
	 * should be updated to reflect any changes. The maximum content
	 * height is maintained in the parent boxes y coordinate. */
	unsigned int max_content_width;

	/** Maximum collapse margin for previous element.
	 *
	 * New block-level boxes being formatted should always check the
	 * value of this member in the parent box's formatting state to
	 * get the correct value for a collapsed margin. */
	unsigned int max_collapse_margin;
};

/** DOM formatter
 *
 * This lives throughout a formatting session and is accessible as the
 * stack context data. In contains information about the renderer's
 * callback and private data, the DOM node stack used for traversing,
 * and the stack of block-level boxes.
 */
struct dom_formatter {
	/** Renderer callback; called for each formatted segment. */
	dom_render_segment_T render;

	/** Renderer callback data. */
	void *render_data;

	/** Index of the current top of the stack of block-level boxes. */
	unsigned int boxes;

	/** The stack used for traversing the DOM tree. */
	struct dom_stack stack;

	/** If >= zero nothing should be displayed. */
	unsigned int display_none;
};


static struct dom_style *get_dom_style(struct dom_node *node);


/** Return a box based on an offset into the stack objects. */
static inline struct dom_formatter_box *
get_dom_formatter_box(struct dom_formatter *formatter, unsigned int offset)
{
	struct dom_formatter_box *box;

	box = (struct dom_formatter_box *) formatter->stack.current->state_objects;
	return box + offset;
}

/** Return the top block-level box. */
static inline struct dom_formatter_box *
get_dom_formatter_boxes(struct dom_formatter *formatter)
{
	return get_dom_formatter_box(formatter, formatter->boxes);
}


/** Format a text string into a series of character run segments.
 *
 * Takes a string of text and formats it into character runs each
 * contained on a single line; no new lines. Furthermore, for
 * non-preformatted content whitespace is normalized and alignment, such
 * as justification, is applied so that each of the resulting segments
 * contained only character runs that the renderer should copy verbatim.
 *
 * Text formatting should abide to the any maximum width and height
 * values for the containing box, block as well as any other kind of box
 * such as inline, etc. For automatic/dynamic width and height it should
 * update the max_content_width value of the containing block-box.
 *
 * If style is NULL, the style of the containing box will be applied
 * when formatting.
 *
 * @param formatter	The DOM formatter.
 * @param box		The box being formatter.
 * @param style		The style to use for formatting.
 * @param text		The text string to format.
 */
static void
dom_format_text(struct dom_formatter *formatter, struct dom_formatter_box *box,
		struct dom_style *style, struct dom_string *text)
{
	struct dom_string string;
	struct dom_segment segment;
	unsigned int max_width = box->offset.right - box->offset.left;
	unsigned int textlen;

	if (max_width < 5)
		max_width = 5;

	memset(&segment, 0, sizeof(segment));

	segment.type = DOM_SEGMENT_CHARRUN;
	segment.element = box->element;
	segment.style = style ? style : box->style;

	segment.offset = box->offset;
	segment.offset.top += box->y;
	segment.offset.left += box->x;

	if (segment.style->white_space == DOM_STYLE_WHITE_SPACE_NORMAL) {
		if (dom_normalize_string(&string, text) != DOM_CODE_OK)
			return;
		text = &string;
	}

	segment.value.text = *text;
	textlen = text->length;

	while (textlen > 0) {
		unsigned int runlen = max_width - box->x;

		if (runlen > textlen)
			runlen = textlen;

		segment.value.text.length = runlen;

		formatter->render(formatter->render_data, &segment);

		segment.value.text.string += runlen;
		textlen -= runlen;
		box->x += runlen;
		segment.offset.left += runlen;

		if (box->max_content_width < box->x)
			box->max_content_width = box->x;

		if (box->x == max_width) {
			box->y++;
			box->x = 0;
			segment.offset = box->offset;
			segment.offset.top += box->y;
			segment.offset.left += box->x;
		}
	}

	/* FIXME: Update box->y? */

	if (text == &string)
		done_dom_string(&string);
}

static enum dom_code
dom_format_element_push(struct dom_stack *stack, struct dom_node *node, void *data)
{
	struct dom_formatter *formatter = stack->current->data;
	struct dom_formatter_box *box = data;
	struct dom_formatter_box *parent = get_dom_formatter_boxes(formatter);

	box->element = node;
	box->style = get_dom_style(node);
	box->parent = formatter->boxes;
	box->offset = parent->offset;

	switch (box->style->display) {
	case DOM_STYLE_DISPLAY_NONE:
		formatter->display_none++;
		return DOM_CODE_OK;

	case DOM_STYLE_DISPLAY_BLOCK:
	case DOM_STYLE_DISPLAY_LIST_ITEM:
		/* Put the new block-level box on the stack. */
		formatter->boxes = stack->depth - 1;

		/* Update the margin offset taking care of vertical
		 * collapsing margins. */
		if (parent->max_collapse_margin > box->style->margin.top)
			box->offset.top += parent->max_collapse_margin;
		else
			box->offset.top += box->style->margin.top;

		parent->x = 0;
		break;

	case DOM_STYLE_DISPLAY_INLINE:
		box->offset.top += box->style->margin.top;
		box->offset.left += parent->x;
		break;

	case DOM_STYLE_DISPLAY_COMPACT:
	case DOM_STYLE_DISPLAY_MARKER:
	case DOM_STYLE_DISPLAY_TABLE:
	case DOM_STYLE_DISPLAY_RUN_IN:
		box->offset.top += box->style->margin.top;
		break;
	}

	box->offset.top += parent->y
		        +  box->style->border.top
			+  box->style->padding.top;

	box->offset.left += box->style->margin.left
			 +  box->style->border.left
			 +  box->style->padding.left;

	/* FIXME: Handle :before content */
	/* FIXME: Handle text-indent */

	return DOM_CODE_OK;
}

static enum dom_code
dom_format_element_pop(struct dom_stack *stack, struct dom_node *node, void *data)
{
	struct dom_formatter *formatter = stack->current->data;
	struct dom_formatter_box *box = data;
	struct dom_formatter_box *parent = get_dom_formatter_box(formatter, box->parent);

	switch (box->style->display) {
	case DOM_STYLE_DISPLAY_NONE:
		formatter->display_none--;
		break;

	case DOM_STYLE_DISPLAY_BLOCK:
	case DOM_STYLE_DISPLAY_LIST_ITEM:
	{
		/* Cleanup the block box stack. */
		unsigned int max_height, max_width;

		if (box->x)
			box->y++;

		/* Calculate the dimensions of the formatted box.
		 *
		 * For each axis, start from the offset, add maximum
		 * formatted content cells, for height add any postponed
		 * margins, and finally add padding, border and margin
		 * offsets. Margins are always postponed for vertical
		 * offsets. */
		max_height = box->offset.top
			   - parent->y
			   + box->y
			   + box->max_collapse_margin
			   + box->style->padding.bottom
			   + box->style->border.bottom
			   - parent->offset.top;

		max_width = box->offset.left
			  + box->max_content_width
			  + box->style->padding.right
			  + box->style->border.right
			  + box->style->margin.right;

		/* Propagate dimensions to the containing block. */
		if (parent->max_content_width < max_width)
			parent->max_content_width = max_width;

		/* Tell the next block about any collapsing. */
		parent->max_collapse_margin = box->style->margin.bottom;

		parent->y += max_height;
		printf("%u %u <- %u %u\n", parent->y, box->y, max_height, parent->max_collapse_margin);

		assert(stack->depth - 1 == formatter->boxes);
		formatter->boxes = box->parent;
		break;
	}
	case DOM_STYLE_DISPLAY_INLINE:
		parent->x += box->x;
	case DOM_STYLE_DISPLAY_COMPACT:
	case DOM_STYLE_DISPLAY_MARKER:
	case DOM_STYLE_DISPLAY_TABLE:
	case DOM_STYLE_DISPLAY_RUN_IN:
		break;
	}

	/* FIXME: Format border segments. */
	/* FIXME: Fill padding areas with bgcolor. */

	return DOM_CODE_OK;
}

static enum dom_code
dom_format_text_push(struct dom_stack *stack, struct dom_node *node, void *data)
{
	struct dom_formatter *formatter = stack->current->data;
	struct dom_formatter_box *box = data;

	if (!formatter->display_none)
		dom_format_text(formatter, &box[-1], NULL, &node->string);

	return DOM_CODE_OK;
}

static enum dom_code
dom_format_document_push(struct dom_stack *stack, struct dom_node *node, void *data)
{
	struct dom_formatter *formatter = stack->current->data;
	struct dom_formatter_box *box = data;

	formatter->boxes = stack->depth - 1;

	/* XXX: Set "fake" default style, to have the style available
	 * if the document only contains a text node. */
	box->style = get_dom_style(node);
	box->offset.top = 1;
	box->offset.left = 3;
	box->offset.bottom = 1000000;
	box->offset.right = 77;

	/* FIXME: This is all wrong but will have to do for now. The
	 * DOM tree that the formatter is given should always have
	 * at least one root element node. */
	box->element = node;

	return DOM_CODE_OK;
}


void
format_dom(struct dom_node *root, dom_render_segment_T fn, void *data)
{
	static struct dom_stack_context_info dom_format_stack_context = {
		/* Object size: */			sizeof(struct dom_formatter_box),
		/* Push: */
		{
			/*				*/ NULL,
			/* DOM_NODE_ELEMENT		*/ dom_format_element_push,
			/* DOM_NODE_ATTRIBUTE		*/ NULL,
			/* DOM_NODE_TEXT		*/ dom_format_text_push,
			/* DOM_NODE_CDATA_SECTION	*/ NULL,
			/* DOM_NODE_ENTITY_REFERENCE	*/ dom_format_text_push,
			/* DOM_NODE_ENTITY		*/ NULL,
			/* DOM_NODE_PROC_INSTRUCTION	*/ NULL,
			/* DOM_NODE_COMMENT		*/ NULL,
			/* DOM_NODE_DOCUMENT		*/ dom_format_document_push,
			/* DOM_NODE_DOCUMENT_TYPE	*/ NULL,
			/* DOM_NODE_DOCUMENT_FRAGMENT	*/ NULL,
			/* DOM_NODE_NOTATION		*/ NULL,
		},
		/* Pop: */
		{
			/*				*/ NULL,
			/* DOM_NODE_ELEMENT		*/ dom_format_element_pop,
			/* DOM_NODE_ATTRIBUTE		*/ NULL,
			/* DOM_NODE_TEXT		*/ NULL,
			/* DOM_NODE_CDATA_SECTION	*/ NULL,
			/* DOM_NODE_ENTITY_REFERENCE	*/ NULL,
			/* DOM_NODE_ENTITY		*/ NULL,
			/* DOM_NODE_PROC_INSTRUCTION	*/ NULL,
			/* DOM_NODE_COMMENT		*/ NULL,
			/* DOM_NODE_DOCUMENT		*/ NULL,
			/* DOM_NODE_DOCUMENT_TYPE	*/ NULL,
			/* DOM_NODE_DOCUMENT_FRAGMENT	*/ NULL,
			/* DOM_NODE_NOTATION		*/ NULL,
		}
	};
	struct dom_formatter formatter = { fn, data };

	init_dom_stack(&formatter.stack, DOM_STACK_FLAG_NONE);
	add_dom_stack_context(&formatter.stack, &formatter, &dom_format_stack_context);
	walk_dom_nodes(&formatter.stack, root);
	done_dom_stack(&formatter.stack);
}

#include "dom/sgml/html/html.h"

static struct dom_style *
get_dom_style(struct dom_node *node)
{
	static struct dom_style li_style = {
		DOM_STYLE_DISPLAY_LIST_ITEM,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style ul_style = {
		DOM_STYLE_DISPLAY_BLOCK,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 1, 1, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style dl_style = {
		DOM_STYLE_DISPLAY_BLOCK,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 1, 1, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style dt_style = {
		DOM_STYLE_DISPLAY_BLOCK,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style dd_style = {
		DOM_STYLE_DISPLAY_BLOCK,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 0, 0, 2, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style head_style = {
		DOM_STYLE_DISPLAY_NONE,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 1, 1, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style h1_style = {
		DOM_STYLE_DISPLAY_BLOCK,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 1, 1, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style h2_style = {
		DOM_STYLE_DISPLAY_BLOCK,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 1, 1, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style p_style = {
		DOM_STYLE_DISPLAY_BLOCK,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 1, 1, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style a_style = {
		DOM_STYLE_DISPLAY_INLINE,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style em_style = {
		DOM_STYLE_DISPLAY_INLINE,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style pre_style = {
		DOM_STYLE_DISPLAY_BLOCK,
		DOM_STYLE_WHITE_SPACE_PRE,
		{ 1, 1, 2, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style default_style = {
		DOM_STYLE_DISPLAY_INLINE,
		DOM_STYLE_WHITE_SPACE_NORMAL,
		{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
		{ 0, NULL },
		{ 0, 0 }
	};
	static struct dom_style *styles[HTML_ELEMENTS];
	static int init;

	if (!init) {
		styles[HTML_ELEMENT_A] = &a_style;
		styles[HTML_ELEMENT_P] = &p_style;
		styles[HTML_ELEMENT_HEAD] = &head_style;
		styles[HTML_ELEMENT_H1] = &h1_style;
		styles[HTML_ELEMENT_H2] = &h2_style;
		styles[HTML_ELEMENT_UL] = &ul_style;
		styles[HTML_ELEMENT_LI] = &li_style;
		styles[HTML_ELEMENT_DL] = &dl_style;
		styles[HTML_ELEMENT_DT] = &dt_style;
		styles[HTML_ELEMENT_DD] = &dd_style;
		styles[HTML_ELEMENT_PRE] = &pre_style;
		styles[HTML_ELEMENT_EM] = &em_style;
		init = 1;
	}

	if (!styles[node->data.element.type])
		return &default_style;

	return styles[node->data.element.type];
}
