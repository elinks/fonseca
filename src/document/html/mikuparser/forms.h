
#ifndef EL__DOCUMENT_HTML_MIKUPARSER_FORMS_H
#define EL__DOCUMENT_HTML_MIKUPARSER_FORMS_H

#include "document/html/mikuparser/parse.h"

struct html_context;

element_handler_T html_button;
element_handler_T html_form;
element_handler_T html_input;
element_handler_T html_select;
element_handler_T html_option;
element_handler_T html_textarea;

#endif
